Los nombres de los alumnos que realizaron el trabajo.
    * Andres Gayoso.
    * Elías Cristaldo.
    * Nilda Gómez.
    * Alvaro Avalos. 
Requerimientos de instalación.
    * Desde el IDE Eclipse
Cómo crear la estructura de Base de datos.
    * Dentro de la carpeta se encuentra un txt llamado "base_de_datos", allí se encuentra el script para la creación de las tablas.
Cómo poblar los datos iniciales necesarios de Base de datos.
    * La tabla suministro debe cargarse con algunos datos. Ejemplo:
    Descripcion: Capiatá; Conectado: true
Cómo compilar y ejecutar los componentes de cada servidor.
    * Para el server es el siguiente archivo
        TCPMultiServer.java
    * Para el cliente es el siguiente archivo
        TCPClient.java
Cómo compilar y ejecutar el/los clientes.
    * Directo se ejecutan los archivos mencionados anteriormente desde el IDE Eclipse
Documentación de un API de servicios ofrecidos por el Servidor.
    La estructura de los paquetes para interactuar con el server es el siguiente
        |estado|mensaje|tipo_operacion|cuerpo|
    1: registrar_consumo
        - tipo_operacion debe registrar el valor 1
        - En el cuerpo se debe mandar un Objeto del tipo suministro
    2: conexion_suministro
        - tipo_operacion debe registrar el valor de 2
        - En el cuerpo se debe mandar un Objeto del tipo suministro (el cual desea conectar). OBS: el suministro con el NIS pasado debe existir en la base de datos
    3: desconexion_suministro
        - tipo_operacion debe registrar el valor de 3
        En el cuerpo se debe mandar un Objeto del tipo suministro (el cual desea desactivar). OBS: el suministro con el NIS pasado debe existir en la base de datos
    4: lista_activos
        - tipo_operacion debe registrar el valor de 4


