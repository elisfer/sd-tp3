package entidad;

public class Consumo {

	private int ID;
	private int NIS;
	private String Descripcion;
	private int Consumo;
	
	public Consumo() {
	}
	
	public Consumo( int ID, int NIS, String Descripcion, int Consumo) {
		this.ID = ID;
		this.NIS = NIS;
		this.Descripcion = Descripcion;
		this.Consumo = Consumo;
	}
	
	public Consumo( int NIS, String Descripcion, int Consumo) {
		this.NIS = NIS;
		this.Descripcion = Descripcion;
		this.Consumo = Consumo;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public int getNIS() {
		return this.NIS;
	}
	
	public String getDescripcion() {
		return this.Descripcion;
	}
	
	public int getConsumo() {
		return this.Consumo;
	}
	
	public void setID( int ID ) {
		this.ID = ID;
	}
	
	public void setNIS( int NIS ) {
		this.NIS = NIS;
	}
	
	public void setDescripcion( String Descripcion ) {
		this.Descripcion = Descripcion;
	}
	
	public void setConsumo( int Consumo ) {
		this.Consumo = Consumo;
	}
	
}
